public class Rotator {
    /**
     * This method rotate the indices of an array.
     *
     * <p>
     *     The method uses a for to go through the array and increase
     *     or decrease the index as indicated.
     * </p>
     *
     * @param data Receive a value of type array.
     * @param n Is the number of indices to rotate.
     * @return Returns an array with the indices rotated.
     */
    public Object[] rotate(Object[] data, int n) {
        Object[] result = new Object[data.length];
        int addedPositions = n % data.length;
        for (int i = 0; i < data.length; i++) {
            if(n >= 0) {
                if(i - addedPositions < 0) {
                    result[i] = data[data.length - addedPositions + i];
                } else {
                    result[i] = data[i - addedPositions];
                }
            }else {
                if(i - addedPositions > data.length - 1) {
                    result[i] = data[-(addedPositions - i) - data.length];
                } else {
                    result[i] = data[i - addedPositions];
                }
            }

        }
        return result;
    }
}
